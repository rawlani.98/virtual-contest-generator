from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

application = Flask(__name__)
application.config['SECRET_KEY'] = '9ca7f6a7202549e85001bce26624df49'
application.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
db = SQLAlchemy(application)
login_manager = LoginManager(application)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'

from flaskblog import routes
