from flask import render_template, url_for, flash, redirect, request, session
from flaskblog import application, db
from flaskblog.forms import LoginForm
from flaskblog.models import User, Post
from flask_login import login_user, current_user, logout_user, login_required
from requests_oauthlib import OAuth2Session
import time
import requests

config = {
    'client_id': '93b0f678bb8fe03ebb35314bb6c1f82c',
    'client_secret': '29b04a608e8d96eb766fd92e1afc71cc',
    'api_endpoint': 'https://api.codechef.com/',
    'authorization_code_endpoint': 'https://api.codechef.com/oauth/authorize',
    'access_token_endpoint': 'https://api.codechef.com/oauth/token',
    'redirect_uri': 'http://www.jugalrawlani.tk/login',
    'website_base_url': 'http://www.jugalrawlani.tk/'
}
posts = [
    {
        'author' : 'Jugal Rawlani',
        'title' : 'Blog Post 1',
        'content' : 'First post content',
        'date_posted' : 'September 6, 2018'
    },
    {
        'author' : 'Vishny Muskawar',
        'title' : 'Blog Post 2',
        'content' : 'Second post content',
        'date_posted' : 'September 7, 2018'
    }
]

@application.route('/')
@application.route('/home')
@login_required
def home():
    return render_template('home.html', posts=posts)

@application.route('/about')
@login_required
def about():
    return render_template('about.html')


@application.route('/login', methods=['GET', 'POST'])
def login():
    global config
    if current_user.is_authenticated:
        return redirect(url_for('home'))

    # user = User.query.filter_by(username='tmw98').first()
    # login_user(user)

    form = LoginForm()
    if form.validate_on_submit():
        return redirect('https://api.codechef.com/oauth/authorize?response_type=code&client_id=93b0f678bb8fe03ebb35314bb6c1f82c&redirect_uri=http%3A%2F%2Fwww.jugalrawlani.tk%2Flogin&state=amM3hAKPnlTvhC5bMzlb0id3CVhDwy')
        # codechef = OAuth2Session(client_id=config['client_id'], redirect_uri=config['redirect_uri'])
        # authorization_url, state = codechef.authorization_url(config['authorization_code_endpoint'])
        # session['oauth_state'] = state
        # return redirect(authorization_url)
        # user = User.query.filter_by(username=form.username.data).first()
        # if user and form.password.data == user.password:
        #     login_user(user, remember=form.remember.data)
        #     next_page = request.args.get('next')
        #     return redirect(next_page) if next_page else redirect(url_for('home'))
        # else:
        #     flash('Login Unsuccessful.', 'danger')
    return render_template('login.html', title='Login', form=form)

@application.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))

@application.route('/callback')
def callback():
    time.sleep(50)
    global config
    config['authorization_code'] = request.args.get('code')
    state = request.args.get('state')
    if config['authorization_code'] == None and session['oauth_state'] != state:
        flash('Login Unsuccessful.', 'danger')
        form = LoginForm()
        return redirect(url_for('login'))

    # requesting for access token
    headers = {
        'content-Type': 'application/json',
    }

    data = '{"grant_type": "authorization_code","code": "%s","client_id": "%s","client_secret": "%s","redirect_uri": "%s"}' % (config['authorization_code'], config['client_id'], config['client_secret'], config['redirect_uri'])

    response = requests.post('https://api.codechef.com/oauth/token', headers=headers, data=data)
    response_json = response.json()
    return response.text

    # retrieving access token from response
    access_token = response_json['result']['data']['access_token']
    token_type = response_json['result']['data']['token_type']

    # getting user details using access token
    headers = {
        'Accept': 'application/json',
        'Authorization': '%s %s' % (token_type, access_token),
    }
    response = requests.get('https://api.codechef.com/users/me', headers=headers)
    return response.text
