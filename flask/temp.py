from celery import Celery
import time

app = Celery('tasks', broker='amqp://localhost//')

@app.task
def abcd(string):
    time.sleep(10)
    return string[::-1]
