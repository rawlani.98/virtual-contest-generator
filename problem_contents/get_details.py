import selenium
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from pyvirtualdisplay import Display

class problem :
	def __init__(self, code):
		# for headless
		self.display = Display(visible = 0, size=(800, 600))
		self.display.start()
		self.link = "https://www.codechef.com/problems/" + code
		self.type = ""
		self.driver = webdriver.Firefox()
		self.driver.get(self.link)

	def get_problem_type(self) :


		# Check if subtasks exist
		try:
			self.driver.find_element_by_id("subtasks")
		except:
			self.contest_type = "Cook-Off"
		else:
			self.contest_type = "Lunch-Time"

		return self.contest_type
		# time_limit
		# source_limit
		# author

	def terminate(self) :
		self.driver.quit()
		self.display.stop()
		

# Problem code
code = input()

object = problem(code)
# print(object.get_problem_type())
object.terminate()

