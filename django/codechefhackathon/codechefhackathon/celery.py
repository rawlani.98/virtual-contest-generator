import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'codechefhackathon.settings')

app = Celery('codechefhackathon')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
