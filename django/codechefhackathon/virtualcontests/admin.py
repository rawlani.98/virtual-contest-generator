from django.contrib import admin
from .models import Problems, Contests, AcceptedSolutions, ActiveVirtualContest, ArchiveVirtualContest, VirtualContest, VirtualContestProblems, Tags, Ranklist, RanklistProblems, ClassifiedTags

admin.site.register(Problems)
admin.site.register(Contests)
admin.site.register(AcceptedSolutions)
admin.site.register(ActiveVirtualContest)
admin.site.register(ArchiveVirtualContest)
admin.site.register(VirtualContest)
admin.site.register(VirtualContestProblems)
admin.site.register(Tags)
admin.site.register(Ranklist)
admin.site.register(RanklistProblems)
admin.site.register(ClassifiedTags)
# admin.site.register(ClassifiedTagsList)