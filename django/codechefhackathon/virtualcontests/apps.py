from django.apps import AppConfig


class VirtualcontestsConfig(AppConfig):
    name = 'virtualcontests'
