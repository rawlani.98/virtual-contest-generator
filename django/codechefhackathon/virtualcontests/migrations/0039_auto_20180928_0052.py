# Generated by Django 2.1.1 on 2018-09-27 19:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('virtualcontests', '0038_auto_20180927_2303'),
    ]

    operations = [
        migrations.AddField(
            model_name='ranklistproblems',
            name='rank',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='virtualcontests.Ranklist'),
        ),
        migrations.AlterField(
            model_name='ranklistproblems',
            name='bestSolutionTime',
            field=models.BigIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='ranklistproblems',
            name='penalty',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='ranklistproblems',
            name='score',
            field=models.FloatField(default=0),
        ),
    ]
