# Generated by Django 2.1.1 on 2018-09-22 18:03

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('virtualcontests', '0024_auto_20180922_1757'),
    ]

    operations = [
        migrations.RenameField(
            model_name='activevirtualcontest',
            old_name='active_contest',
            new_name='contest',
        ),
        migrations.RenameField(
            model_name='archivevirtualcontest',
            old_name='archived_contest',
            new_name='contest',
        ),
        migrations.AlterField(
            model_name='virtualcontest',
            name='date_end',
            field=models.DateTimeField(default=datetime.datetime(2018, 9, 22, 21, 3, 35, 140605, tzinfo=utc)),
        ),
    ]
