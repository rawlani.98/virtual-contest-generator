# Generated by Django 2.1.1 on 2018-09-30 09:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('virtualcontests', '0052_virtualcontest_last_refresh'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activevirtualcontest',
            name='user',
            field=models.CharField(default=' ', max_length=50),
        ),
        migrations.AlterField(
            model_name='archivevirtualcontest',
            name='user',
            field=models.CharField(default=' ', max_length=50),
        ),
        migrations.AlterField(
            model_name='ranklistproblems',
            name='problem_code',
            field=models.CharField(default=' ', max_length=10),
        ),
    ]
