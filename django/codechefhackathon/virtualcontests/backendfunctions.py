import requests
import datetime
import pytz
from django.http import HttpResponse
from .models import ActiveVirtualContest, Problems, AcceptedSolutions, Contests, ArchiveVirtualContest, VirtualContest, VirtualContestProblems, Ranklist, RanklistProblems, ClassifiedTags, Tags
from django.db.models.aggregates import Count
from random import randint
import json
from bs4 import BeautifulSoup
from django.utils import timezone
from users.models import Profile
import time

def getContestList(access_token):
    headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer %s' % (access_token),
    }
    params = (
        ('status', 'past'),
    )

    response = requests.get('https://api.codechef.com/contests', headers=headers, params=params)
    return response.json()

def checkContestAcceptable(code):
    return 'LTIME' not in code and 'COOK' not in code

def checkContestExists(database_list, code):
    return database_list.filter(contest_code=code).first()

def getContestJSON(access_token, code):
    headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer %s' % (access_token),
    }
    contest_details = requests.get('https://api.codechef.com/contests/%s' % (code), headers=headers)
    print(contest_details.text)
    print("GOT JSON")
    contest_json = contest_details.json()

    return contest_json

def extractContestDetails(contest_json):
    format_str = '%Y-%m-%d %H:%M:%S' # The format
    temp_start_time = contest_json['result']['data']['content']['startDate']
    temp_end_time = contest_json['result']['data']['content']['endDate']
    start_time_datetime = datetime.datetime.strptime(temp_start_time, format_str)
    end_time_datetime = datetime.datetime.strptime(temp_end_time, format_str)
    contest_code = contest_json['result']['data']['content']['code']
    contest_name = contest_json['result']['data']['content']['name']
    return contest_code, contest_name, start_time_datetime, end_time_datetime

def extractProblemDetails(problem_json):
    return problem_json['problemCode'], problem_json['problemName'], problem_json['successfulSubmissions'], problem_json['accuracy'], problem_json['type']

def isContestParent(contest_json):
    return contest_json['result']['data']['content']['isParent']

def checkProblemExists(database_list, code):
    return database_list.filter(problem_code=code).first()

def extractContestProblems(contest_json):
    return contest_json['result']['data']['content']['problemsList']

def checkContestProblemExists(database_list, contest):
    return database_list.filter(problem_contest=contest).first()

def getPracticeProblemsList(code):
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0'
    }
    response = requests.get('https://www.codechef.com/problems/%s' % (code), headers=headers)
    print(response.status_code)
    soup_obj = BeautifulSoup(response.text, 'html.parser')
    table_tag = soup_obj.find('table', {'class': 'dataTable'})
    tr_tags = table_tag.findChild('tbody').findChildren('tr', {'class': 'problemrow'})
    problem_list = []
    for tr in tr_tags:
        td_tags = tr.findChildren('td')
        problem = {}
        problem['problemName'] = td_tags[0].text.strip()
        problem['problemCode'] = td_tags[1].text.strip()
        problem['successfulSubmissions'] = int(td_tags[2].text.strip())
        problem['accuracy'] = float(td_tags[3].text.strip())
        if td_tags[2].findChild('div')['data-tooltip'].strip() == '':
            problem['type'] = 'COOK'
        else:
            problem['type'] = 'LTIME'
        problem_list.append(problem)
    return problem_list


def getEasyQuestion(user, type):
    problem_code_all = Problems.objects.filter(problem_difficulty='easy', problem_type=type).values_list('problem_code', flat=True)
    problem_code_user = AcceptedSolutions.objects.filter(username='user').all().values_list('question__problem_code', flat=True)
    problem_set = problem_code_all.difference(problem_code_user)
    count = len(problem_set)
    random_index = randint(0, count - 1)
    problem_code = problem_set[random_index]
    problem = Problems.objects.filter(problem_code=problem_code).first()
    return problem
    # return problem_code

def getSchoolQuestion(user, type):
    problem_code_all = Problems.objects.filter(problem_difficulty='school', problem_type=type).values_list('problem_code', flat=True)
    problem_code_user = AcceptedSolutions.objects.filter(username='user').all().values_list('question__problem_code', flat=True)
    problem_set = problem_code_all.difference(problem_code_user)
    count = len(problem_set)
    random_index = randint(0, count - 1)
    problem_code = problem_set[random_index]
    problem = Problems.objects.filter(problem_code=problem_code).first()
    return problem
    # return problem_code

def getMediumQuestion(user, type):
    problem_code_all = Problems.objects.filter(problem_difficulty='medium', problem_type=type).values_list('problem_code', flat=True)
    problem_code_user = AcceptedSolutions.objects.filter(username='user').all().values_list('question__problem_code', flat=True)
    problem_set = problem_code_all.difference(problem_code_user)
    count = len(problem_set)
    random_index = randint(0, count - 1)
    problem_code = problem_set[random_index]
    problem = Problems.objects.filter(problem_code=problem_code).first()
    return problem

def getHardQuestion(user, type):
    problem_code_all = Problems.objects.filter(problem_difficulty='hard', problem_type=type).values_list('problem_code', flat=True)
    problem_code_user = AcceptedSolutions.objects.filter(username='user').all().values_list('question__problem_code', flat=True)
    problem_set = problem_code_all.difference(problem_code_user)
    count = len(problem_set)
    random_index = randint(0, count - 1)
    problem_code = problem_set[random_index]
    problem = Problems.objects.filter(problem_code=problem_code).first()
    return problem

def createContest(user, contest_type):
    contestType = contest_type
    user_created = user
    virtual_contest = VirtualContest(contestType=contest_type, user_created=user_created, contest_code='CUSTOM')
    virtual_contest.save()

    if contest_type == 'COOK':
        virtual_contest.date_end -= timezone.timedelta(hours=0.5)
        virtual_contest.save()

    # for i in virtual_contest.question_count:
    contest_problem = VirtualContestProblems(virtual_contest=virtual_contest, problem=getSchoolQuestion(user, contest_type))
    contest_problem.save()
    contest_problem = VirtualContestProblems(virtual_contest=virtual_contest, problem=getEasyQuestion(user, contest_type))
    contest_problem.save()
    contest_problem = VirtualContestProblems(virtual_contest=virtual_contest, problem=getMediumQuestion(user, contest_type))
    contest_problem.save()
    while True:
        contest_problem_possible = VirtualContestProblems(virtual_contest=virtual_contest, problem=getMediumQuestion(user, contest_type))
        if contest_problem_possible.problem.problem_code != contest_problem.problem.problem_code:
            break 
    contest_problem = contest_problem_possible
    contest_problem.save()
    contest_problem = VirtualContestProblems(virtual_contest=virtual_contest, problem=getHardQuestion(user, contest_type))
    contest_problem.save()

    active_contest = ActiveVirtualContest(contest=virtual_contest, user=user.username)
    return active_contest

def getSubmissionLink(code):
    link = "https://www.codechef.com/problems/" + code;
    return link

def createvirtualcontestltime(user):
    contest = createContest(user, 'LTIME')
    contest.save()
    return True
    # return HttpResponse('Create New')

def createvirtualcontestcook(user):
    contest = createContest(user, 'COOK')
    contest.save()
    return True

def createvirtualcontestpast(user, contestName):
    contest_obj = Contests.objects.filter(contest_code=contestName).first()
    print(contest_obj)
    if contest_obj == None:
        return False
    if 'LTIME' in contest_obj.contest_code:
        contest_type = 'LTIME'
    elif 'COOK' in contest_obj.contest_code:
        contest_type = 'COOK'
    else:
        return False

    contestType = contest_type
    user_created = user
    virtual_contest = VirtualContest(contestType=contest_type, user_created=user_created)
    virtual_contest.past_contest = True
    virtual_contest.contest_code = contest_obj.contest_code
    virtual_contest.save()
    if contest_type == 'COOK':
        virtual_contest.date_end -= timezone.timedelta(hours=0.5)
        virtual_contest.save()

    problems = Problems.objects.filter(problem_contest=contest_obj)
    for problem in problems:
        contest_problem = VirtualContestProblems(virtual_contest=virtual_contest, problem=problem)
        contest_problem.save()
    active_contest = ActiveVirtualContest(contest=virtual_contest, user=user.username)
    active_contest.save()
    return True

def checkActiveVirtualContest(user):
    # return active virtual contest
    return ActiveVirtualContest.objects.filter(user=user.username).first()

def checkVirtualContestStatus(active_contest):
    if timezone.now() > active_contest.contest.date_end:
        endVirtualContest(active_contest)
        return True
    return False

def endVirtualContest(active_contest):
    archived_contest = ArchiveVirtualContest(contest=active_contest.contest, user=active_contest.user)
    for problem in VirtualContestProblems.objects.filter(virtual_contest=active_contest.contest):
        problem.problem_successful_submissions = RanklistProblems.objects.filter(problem_code=problem.problem.problem_code).count()
        problem.save()
    archived_contest.contest.date_end = timezone.now()
    archived_contest.contest.save()
    archived_contest.save()
    active_contest.delete()
    return True

def getQuestionSubmisionStatus(access_token, username, problem_code, offset, acceptedStatus):
    headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer %s' % (access_token),
    }
    if acceptedStatus == None:
        params = (
            ('username', username),
            ('problemCode', problem_code),
            ('contestCode', 'PRACTICE')
        )
    else:
        params = (
            ('username', username),
            ('problemCode', problem_code),
            ('result', 'AC'),
            ('contestCode', 'PRACTICE')
        )
    response = requests.get('https://api.codechef.com/submissions', headers=headers, params=params)
    return response.json()

def checkAcceptedStatusCook(access_token, active_contest, problem):
    response = getQuestionSubmisionStatus(access_token, active_contest.user, problem.problem.problem_code, 0, 'AC')
    print(response)
    if response['result']['data']['message'] == "no submissions found for this search":
        return False

    contents = response['result']['data']['content']
    start_time = active_contest.contest.date_started.replace(tzinfo=None) + timezone.timedelta(hours=5.5)
    end_time = active_contest.contest.date_end.replace(tzinfo=None) + timezone.timedelta(hours=5.5)
    submit_check = 0
    for content in contents:
        time = content['date']
        time = datetime.datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
        if time < start_time:
            break
        submit_time = time
        submit_check = 1
    # print(start_time)
    # print(end_time)
    # print(submit_time)

    if(submit_check and submit_time > start_time and submit_time < end_time):
        problem.problem_score = 1
        response = getQuestionSubmisionStatus(access_token, active_contest.user, problem.problem.problem_code, 0, None)
        contents = response['result']['data']['content']

        for content in contents:
            print(1)
            check_time = content['date']
            check_time = datetime.datetime.strptime(check_time, "%Y-%m-%d %H:%M:%S")
            if check_time < start_time:
                break
            if check_time < submit_time:
                problem.problem_penalty += 1

        # Update problem total time
        problem.problem_time = problem.problem_penalty * 60 * 20
        question_solve_time = (submit_time - start_time).total_seconds()
        problem.problem_time += question_solve_time
        problem.save()

        return True

    return False

def checkAcceptedStatusLtime(access_token, active_contest, problem):
    response = getQuestionSubmisionStatus(access_token, active_contest.user, problem.problem.problem_code, 0, 'AC')
    if response['result']['data']['message'] == "no submissions found for this search":
        return False

    print(response)
    # print(1)
    contents = response['result']['data']['content']
    start_time = active_contest.contest.date_started.replace(tzinfo=None) + timezone.timedelta(hours=5.5)
    end_time = active_contest.contest.date_end.replace(tzinfo=None) + timezone.timedelta(hours=5.5)

    for content in contents:
        submit_time = content['date']
        submit_time = datetime.datetime.strptime(submit_time, "%Y-%m-%d %H:%M:%S")
        if submit_time > end_time or submit_time < start_time:
            return True
        score = getLtimeScore(content['id'])
        score = float(score)
        print(problem.problem_score)
        print(score)
        if problem.problem_score < score:
            problem.problem_score = score
            problem.problem_penalty = 0
            question_solve_time = (submit_time - start_time).total_seconds()
            problem.problem_time = question_solve_time
            problem.save()

def updateTotalTime(active_contest, problem):
    contest_problems = VirtualContestProblems.objects.filter(virtual_contest=active_contest.contest)
    contest = active_contest.contest
    contest.total_time = 0
    contest.total_penalty = 0
    for problem in contest_problems:
        contest.total_time += problem.problem_time
        contest.total_penalty += problem.problem_penalty
    contest.save()

def getSubmissionStatusCook(access_token, active_contest):
    contest_problems = VirtualContestProblems.objects.filter(virtual_contest=active_contest.contest)
    contest = active_contest.contest
    for problem in contest_problems:
        if problem.problem_score == 0:
            checkAcceptedStatusCook(access_token, active_contest, problem)
            contest.total_score += problem.problem_score
    contest.save()
    updateTotalTime(active_contest, problem)
    return True

def getLtimeScore(submissionID):
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0'
    }
    response = requests.get('https://www.codechef.com/viewsolution/' + str(submissionID), headers=headers)
    soup_obj = BeautifulSoup(response.text, 'html.parser')
    json_obj = json.loads(soup_obj.find('pre', {'id': 'meta-info'}).text)
    return json_obj['data']['solutionScore']

def getSubmissionStatusLtime(access_token, active_contest):
    contest_problems = VirtualContestProblems.objects.filter(virtual_contest=active_contest.contest)
    contest = active_contest.contest
    contest.total_score = 0
    for problem in contest_problems:
        if problem.problem_score != 100:
            checkAcceptedStatusLtime(access_token, active_contest, problem)
            contest.total_score += problem.problem_score
        else:
            contest.total_score += 100
    contest.save()
    updateTotalTime(active_contest, problem)
    return True

def getSubmissionStatus(access_token, active_contest):
    contest = active_contest.contest
    current_time = timezone.now()
    time_from_last_refresh = (current_time - contest.last_refresh).total_seconds()
    print(time_from_last_refresh)
    if time_from_last_refresh < 120:
        return False
    contest.last_refresh = timezone.now()
    # print('here')

    contest.save()
    active_contest.save()

    if active_contest.contest.contestType == 'COOK':
        getSubmissionStatusCook(access_token, active_contest)
    else:
        getSubmissionStatusLtime(access_token, active_contest)
    if active_contest.contest.past_contest:
        updateRank(access_token, active_contest)

def updateGlobalRank(access_token, active_contest):
    contest = active_contest.contest
    # problem = VirtualContestProblems.objects.filter(virtual_contest=contest).first().problem()
    # contest_current = problem.problem_contest
    contest_current = Contests.objects.filter(contest_code=contest.contest_code).first()

    # print(len(Ranklist.objects.filter(contest=contest_current, total_score=float(contest.total_score), total_time__lt=contest.total_time).all()))
    contest_interval = Ranklist.objects.filter(contest=contest_current, total_score=float(contest.total_score), total_time__lt=contest.total_time).last()
    if contest_interval is None:
        check = Ranklist.objects.filter(contest=contest_current, total_score__gt=float(contest.total_score)).last()
        if check is None:
            contest.global_rank = 1
        else:
            contest.global_rank = check.rank + 1
    else:
        contest.global_rank = contest_interval.rank + 1
    contest.save()
    # print('done')
    return True

def updateCountryRank(access_token, active_contest):
    contest = active_contest.contest
    user = active_contest.user
    profile = Profile.objects.filter(username=user).first()
    contest_current = Contests.objects.filter(contest_code=contest.contest_code).first()
    # problem = VirtualContestProblems.objects.filter(virtual_contest=contest).first().problem()
    # contest_current = contest.contest_code
    # contest_current = contest.contest_code
    country = profile.country
    contest_interval = Ranklist.objects.filter(contest=contest_current, country=country, total_score=float(contest.total_score), total_time__lt=contest.total_time).all()
    if contest_interval is None:
        check = Ranklist.objects.filter(contest=contest_current, country=country, total_score__gt=float(contest.total_score)).all()
        if check is None:
            contest.country_rank = 1
        else:
            check = Ranklist.objects.filter(contest=contest_current, country=country, total_score__gt=float(contest.total_score)).all()
            contest.country_rank = len(check) + 1
    else:
        contest.country_rank = len(contest_interval) + 1
        check = Ranklist.objects.filter(contest=contest_current, country=country, total_score__gt=float(contest.total_score)).all()
        contest.country_rank += len(check)
    contest.save()
    # print('done')
    return True

def updateOrganizationRank(access_token, active_contest):
    contest = active_contest.contest
    user = active_contest.user
    profile = Profile.objects.filter(username=user).first()
    if profile.organization is None:
        return False

    organization = profile.organization
    # problem = VirtualContestProblems.objects.filter(virtual_contest=contest).first().problem()
    # contest_current = problem.problem_contest
    # contest_current = contest.contest_code
    contest_current = Contests.objects.filter(contest_code=contest.contest_code).first()
    contest_interval = Ranklist.objects.filter(contest=contest_current, organization=organization, total_score=float(contest.total_score), total_time__lt=contest.total_time).all()
    if contest_interval is None:
        check = Ranklist.objects.filter(contest=contest_current, organization=organization, total_score__gt=float(contest.total_score)).all()
        if check is None:
            contest.school_rank = 1
        else:
            check = Ranklist.objects.filter(contest=contest_current, organization=organization, total_score__gt=float(contest.total_score)).all()
            contest.country_rank = len(check) + 1
    else:
        contest.school_rank = len(contest_interval) + 1
        check = Ranklist.objects.filter(contest=contest_current, organization=organization, total_score__gt=float(contest.total_score)).all()
        contest.country_rank += len(check)
    contest.save()
    # print('done')
    return True

def updateRank(access_token, active_contest):
    if active_contest.contest.total_score == 0:
        return False
    updateGlobalRank(access_token, active_contest)
    updateCountryRank(access_token, active_contest)
    updateOrganizationRank(access_token, active_contest)
    print(active_contest.contest.global_rank)
    print(active_contest.contest.country_rank)
    print(active_contest.contest.school_rank)
    print('done')
    return True

def updateRanklists(access_token, contest_code):
    i = 0
    while True:
        response_json = getRanklist(access_token, contest_code, i)
        while response_json['status'] == 'error':
            time.sleep(60)
            print(response_json)
            response_json = getRanklist(access_token, contest_code, i)
        problem_solved_list = []
        if response_json['result']['data']['message'] != 'Rank List successfully fetched.':
            print("No more ranks to fetch")
            break
        ranklist = response_json['result']['data']['content']
        contests = Contests.objects.all()
        problems = Problems.objects.all()

        temp_list = [ranklist[i * 500:(i + 1) * 500] for i in range((len(ranklist) + 500 - 1) // 500 )]
        for temp in temp_list:
            ranklist_broken = temp
            rank_objects_list = []
            for rank in ranklist_broken:
                rank_no = int(rank['rank'])
                username = rank['username']
                total_time = rank['totalTime'].split(':')
                total_time = int(total_time[2]) + int(total_time[1]) * 60 + int(total_time[0]) * 3600
                penalty = int(rank['penalty'])
                country = rank['country']
                organization = rank['institution']
                organizationType = rank['institutionType']
                contest = contests.filter(contest_code=rank['contestCode']).first()
                rating = int(rank['rating'])
                total_score = float(rank['totalScore'])
                rank_data = {
                    'rank': rank_no,
                    'username': username,
                    'total_time': total_time,
                    'penalty': penalty,
                    'country': country,
                    'organization': organization,
                    'organizationType': organizationType,
                    'contest': contest,
                    'rating': rating,
                    'total_score': total_score
                }
                rank_objects_list.append(Ranklist(**rank_data))
                print("Added Rank %d %s" % (rank_no, username))
                # problem_solved_list = []
                problemSolved = rank['problemScore']
                if problemSolved == None:
                    continue
                for problem in problemSolved:
                    problem_obj = problems.filter(problem_code=problem['problemCode']).first()
                    bestSolutionTime = problem['bestSolutionTime']
                    penalty = problem['penalty']
                    score = problem['score']
                    problem_json = {
                        'contest': contest,
                        'rank': rank_no,
                        'problem_code': problem_obj.problem_code,
                        'bestSolutionTime': bestSolutionTime,
                        'penalty': penalty,
                        'score': score,
                    }
                    problem_solved_list.append(RanklistProblems(**problem_json))
            Ranklist.objects.bulk_create(rank_objects_list)

                # problem_list_add = [ranklist[i * 500:(i + 1) * 500] for i in range((len(ranklist) + 500 - 1) // 500 )]
        j = 0
        print(len(problem_solved_list))
        while True:
            if (j + 500) >= len(problem_solved_list):
                add = problem_solved_list[j:len(problem_solved_list)]
                RanklistProblems.objects.bulk_create(add)
                break
            else:
                add = problem_solved_list[j:j+500]
                RanklistProblems.objects.bulk_create(add)
            j += 500
        problem_solved_list.clear()
        i += 1500
    print(len(problem_solved_list))

def updateSuccessfulSubmissions(active_contest):
    virtual_contest = active_contest.contest
    if not virtual_contest.past_contest:
        return False
    contest_code = virtual_contest.contest_code
    contest = Contests.objects.filter(contest_code=contest_code).first()
    virtualcontestproblems = VirtualContestProblems.objects.filter(virtual_contest=virtual_contest)
    ranklistproblems = RanklistProblems.objects.filter(contest=contest)
    seconds_elapsed = int((timezone.now().replace(tzinfo=None) - virtual_contest.date_started.replace(tzinfo=None)).total_seconds())
    print(seconds_elapsed)
    compare_time = contest.contest_start_time + timezone.timedelta(seconds=seconds_elapsed) - timezone.timedelta(hours=5.5)
    compare_time = compare_time.timestamp()
    print(compare_time)
    for virtualcontestproblem in virtualcontestproblems:
        virtualcontestproblem.problem_successful_submissions = ranklistproblems.filter(bestSolutionTime__lte=compare_time, problem_code=virtualcontestproblem.problem.problem_code).count()
        virtualcontestproblem.save()

def updateClassifiedTags():
    ClassifiedTags.objects.all().delete()
    file = open('classified_tags.txt', 'r')
    dic = {}
    classified_tags = []
    i = 0
    add_tag_bulk = []
    total = 0
    y = []

    for r in file:
        temp = r.split(' = ')
        temp[0] = temp[0].strip()
        temp[1] = temp[1].strip()
        if temp[0] not in classified_tags:
            classified_tags.append(temp[0])
            print(temp[0])
        if temp[1] not in dic:
            dic[temp[1]] = []
        dic[temp[1]].append(temp[0])

    problems = Problems.objects.all()
    for problem in problems:
        count = 0
        tags = Tags.objects.filter(problem=problem).all()
        x = []
        print(problem.problem_code)
        for tag in tags:
            tag = tag.tag
            if tag in dic:
                for j in dic[tag]:
                    x.append(j)
                    count += 1
        add = []
        if count > 0:
            weight = 1 / float(count)
            total += 1
        for tag_present in x:
            if tag_present not in add:
                add.append(tag_present)
                print(tag_present)
                add_tag = {
                    'problem': problem,
                    'tag': tag_present,
                    'weight': weight*(x.count(tag_present)),
                }
                i += 1
                add_tag_bulk.append(ClassifiedTags(**add_tag))
                if i >= 500:
                    ClassifiedTags.objects.bulk_create(add_tag_bulk)
                    i = 0
                    add_tag_bulk = []
                    print('500')
                    # time.sleep(2)
    if i != 0:
        ClassifiedTags.objects.bulk_create(add_tag_bulk)

    print(total)

def updateClassifiedTagsList():
    print('here')
    ClassifiedTagsList.objects.all().delete()
    file = open('classified_tags.txt', 'r')
    l = []
    for r in file:
        temp = r.split(' = ')
        temp = temp[0].strip()
        if temp not in l:
            l.append(temp)
    for tag in l:
        print(tag)
        a = ClassifiedTagsList(tag=tag)
        a.save()


def getRanklist(access_token, contest_code, offset):
    headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer %s' % (access_token),
    }
    params = (
        ('offset', str(offset)),
    )
    response = requests.get('https://api.codechef.com/rankings/%s' % (contest_code) , headers=headers, params=params)
    response_json = response.json()
    return response_json

def isRanklistDone(response_json):
    if response_json['result']['data']['message'] != 'Rank List successfully fetched.':
        return True
    return False

def updateRanklistLtime(access_token, contest_code):
    i = 0
    while True:
        response_json = getRanklist(access_token, contest_code, i)
        while response_json['status'] == 'error':
            time.sleep(60)
            response_json = getRanklist(access_token, contest_code, i)
        problem_solved_list = []
        if isRanklistDone(response_json):
            break
        ranklist = response_json['result']['data']['content']
        contests = Contests.objects.all()
        problems = Problems.objects.all()
        broken_lists = [ranklist[i * 500:(i + 1) * 500] for i in range((len(ranklist) + 500 - 1) // 500 )]

        for ranklist_broken in broken_lists:
            rank_objects_list = []
            for rank in ranklist_broken:
                rank_no = int(rank['rank'])
                username = rank['username']
                penalty = int(rank['penalty'])
                country = rank['country']
                organization = rank['institution']
                organizationType = rank['institutionType']
                contest = contests.filter(contest_code=rank['contestCode']).first()
                rating = int(rank['rating'])
                total_score = float(rank['totalScore'])
                start_time_epoch = time.mktime(contest.contest_start_time.timetuple())
                print("Added Rank %d %s" % (rank_no, username))
                # problem_solved_list = []
                total_time = 0
                problemSolved = rank['problemScore']
                if problemSolved == None:
                    continue
                for problem in problemSolved:
                    problem_obj = problems.filter(problem_code=problem['problemCode']).first()
                    rank_bestSolutionTime = problem['bestSolutionTime']
                    total_time += rank_bestSolutionTime - start_time_epoch
                    rank_penalty = problem['penalty']
                    rank_score = problem['score']
                    problem_json = {
                        'contest': contest,
                        'rank': rank_no,
                        'problem_code': problem_obj.problem_code,
                        'bestSolutionTime': rank_bestSolutionTime,
                        'penalty': rank_penalty,
                        'score': rank_score,
                    }
                    problem_solved_list.append(RanklistProblems(**problem_json))
                rank_data = {
                    'rank': rank_no,
                    'username': username,
                    'total_time': total_time,
                    'penalty': penalty,
                    'country': country,
                    'organization': organization,
                    'organizationType': organizationType,
                    'contest': contest,
                    'rating': rating,
                    'total_score': total_score,
                    'total_time': total_time
                }
                rank_objects_list.append(Ranklist(**rank_data))
            Ranklist.objects.bulk_create(rank_objects_list)
        j = 0
        print(len(problem_solved_list))
        while True:
            if (j + 500) >= len(problem_solved_list):
                add = problem_solved_list[j:len(problem_solved_list)]
                RanklistProblems.objects.bulk_create(add)
                break
            else:
                add = problem_solved_list[j:j+500]
                RanklistProblems.objects.bulk_create(add)
            j += 500
        problem_solved_list.clear()
        i += 1500


def isAccessTokenValid(user):
    current_time = timezone.now()
    profile = Profile.objects.filter(user=user).first()
    access_token_validity_time = profile.last_login + timezone.timedelta(hours=1)
    print(current_time)
    print(access_token_validity_time)
    if current_time >= access_token_validity_time:
        return False
    return True
