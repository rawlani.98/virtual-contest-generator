from django.urls import path
from . import views

urlpatterns = [
    path('', views.dashboard, name="virtualcontest-dashboard"),
    path('dashboard/', views.dashboard, name="virtualcontest-dashboard"),
    path('updateproblemsdatabase/', views.updateproblemsdatabase, name='update-problems'),
    # path('downloadproblems/', views.downloadproblems, name='download-problems')
    path('active/', views.active, name="virtualcontest-active"),
    path('createvirtualcontest/', views.createvirtualcontest, name="virtualcontestcook-create"),
    path('end/', views.end, name='virtualcontest-end'),
    path('ranks/', views.ranks, name='virtualcontest-ranks'),
    path('problem/', views.problem),
    path('showproblem/', views.showproblem, name='virtualcontest-showproblem'),
    path('archivedredirect/', views.archivedredirect),
    path('archived/', views.archived, name='virtualcontest-archived')
]
