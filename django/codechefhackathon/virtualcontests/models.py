from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

def assignDate():
    return timezone.now() + timezone.timedelta(hours=3)

class Contests(models.Model):
    contest_code = models.CharField(max_length=10)
    contest_name = models.CharField(max_length=50)
    contest_start_time = models.DateTimeField()
    contest_end_time = models.DateTimeField()

    def __str__(self):
        return 'Contest : %s' % (self.contest_code)

class Problems(models.Model):
    problem_code = models.CharField(max_length=10)
    problem_name = models.TextField(null=True)
    contest_successful_submissions = models.IntegerField(null=True)
    contest_accuracy = models.FloatField(null=True)
    practice_successful_submissions = models.IntegerField(null=True)
    practice_accuracy = models.FloatField(null=True)
    problem_directory = models.FileField(null=True, upload_to='questions')
    problem_contest = models.ForeignKey(Contests, on_delete=models.CASCADE)
    problem_difficulty = models.CharField(max_length=10, null=True)
    problem_type = models.CharField(max_length=10, null=True)

    def __str__(self):
        return 'Problem : %s in %s' % (self.problem_code, self.problem_contest.contest_code)

class VirtualContest(models.Model):
    # contestType = models.ForeignKey(Contests, on_delete=models.CASCADE)
    contestType = models.CharField(max_length=10)
    date_started = models.DateTimeField(default=timezone.now)
    date_end = models.DateTimeField(default=assignDate)
    user_created = models.ForeignKey(User, on_delete=models.CASCADE)
    total_score = models.IntegerField(default=0)
    problem_count = models.IntegerField(default=5)
    total_time = models.IntegerField(default=0)
    total_penalty = models.IntegerField(default=0)
    global_rank = models.IntegerField(default=0)
    country_rank = models.IntegerField(default=0)
    school_rank = models.IntegerField(default=0)
    past_contest = models.BooleanField(default=False)
    contest_code = models.CharField(max_length=10, null=True)
    last_refresh = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return 'Contest : {} {} {}'.format(self.contestType, self.user_created.username, self.date_started)

class VirtualContestProblems(models.Model):
    problem = models.ForeignKey(Problems, on_delete=models.CASCADE)
    virtual_contest = models.ForeignKey(VirtualContest, on_delete=models.CASCADE)
    problem_penalty = models.IntegerField(default=0, null=True)
    problem_score = models.IntegerField(default=0, null=True)
    problem_time = models.IntegerField(default=0, null=True)
    problem_successful_submissions = models.IntegerField(default=0)

    def __str__(self):
        return 'Problem : {} by {}'.format(self.problem.problem_code, self.virtual_contest.user_created)


class ActiveVirtualContest(models.Model):
    user = models.CharField(max_length=50, default='')
    contest = models.ForeignKey(VirtualContest, on_delete=models.CASCADE)

    def __str__(self):
        return 'User : {}'.format(self.user)

class ArchiveVirtualContest(models.Model):
    user = models.CharField(max_length=50, default='')
    contest = models.ForeignKey(VirtualContest, on_delete=models.CASCADE)

    def __str__(self):
        return 'User : {}'.format(self.user)

class AcceptedSolutions(models.Model):
    question = models.ForeignKey(Problems, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    username = models.CharField(max_length=50, null=True)

    def __str__(self):
        return 'Problem %s solved by %s' % (self.question.problem_code, self.user.username)

class Tags(models.Model):
    problem = models.ForeignKey(Problems, on_delete=models.CASCADE)
    tag = models.CharField(max_length=50)

    def __str__(self):
        return '%s at %s' % (self.tag, self.problem.problem_code)

class Ranklist(models.Model):
    rank = models.IntegerField()
    username = models.CharField(max_length=50)
    total_time = models.IntegerField()
    penalty = models.IntegerField()
    country = models.TextField(null=True)
    organization = models.TextField(null=True)
    organizationType = models.TextField(null=True)
    contest = models.ForeignKey(Contests, on_delete=models.CASCADE)
    rating = models.IntegerField()
    total_score = models.FloatField()

    def __str__(self):
        return '%d of %s' % (self.rank, self.contest.contest_code)

class RanklistProblems(models.Model):
    contest = models.ForeignKey(Contests, on_delete=models.CASCADE)
    # problem = models.ForeignKey(Problems, on_delete=models.CASCADE)
    rank = models.IntegerField(default=0)
    problem_code = models.CharField(max_length=10, default='')
    bestSolutionTime = models.BigIntegerField(default=0)
    penalty = models.IntegerField(default=0)
    score = models.FloatField(default=0)

    def __str__(self):
        return '%s of %s' % (self.problem_code, self.contest.contest_code)

class ClassifiedTags(models.Model):
    problem = models.ForeignKey(Problems, on_delete=models.CASCADE, default=None)
    tag = models.CharField(max_length=30, null=True)
    weight = models.FloatField(default=0)

    def  __str__(self):
        return '%s' % (self.tag)

