from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone
import os
from django.core.files.storage import default_storage
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth import logout
from . import backendfunctions
from .models import Contests, Problems, ActiveVirtualContest, ArchiveVirtualContest, VirtualContest, VirtualContestProblems
from users.models import Profile

@login_required(login_url='user-login')
def dashboard(request):
    print(backendfunctions.isAccessTokenValid(request.user))
    if not backendfunctions.isAccessTokenValid(request.user):
        logout(request)
        messages.info(request, 'Access Token Has Expired! Please login again to continue')
        return redirect('user-login')
    # backendfunctions.updateClassifiedTagsList()
    contests = Contests.objects.all().exclude(contest_code='PRACTICE')
    active_contests = ActiveVirtualContest.objects.filter(user=request.user.username).order_by('-id')
    print(active_contests)
    archived_contests = ArchiveVirtualContest.objects.filter(user = request.user.username).order_by('-id')
    context = {
        'contests': contests,
        'active_contests': active_contests,
        'archived_contests': archived_contests,
    }
    return render(request, 'virtualcontests/dashboard.html', context)

@login_required(login_url='user-login')
def createvirtualcontest(request):
    if not backendfunctions.isAccessTokenValid(request.user):
        logout(request)
        messages.info(request, 'Access Token Has Expired! Please login again to continue')
        return redirect('user-login')

    user = request.user
    if backendfunctions.checkActiveVirtualContest(user) != None:
        return redirect('virtualcontest-active')
    contestType = request.GET['contestType']
    contestName = request.GET['contestName']
    if contestType == 'CUSTOM':
        if contestName == 'LTIME':
            backendfunctions.createvirtualcontestltime(user)
        elif contestName == 'COOKOFF':
            backendfunctions.createvirtualcontestcook(user)
    elif contestType == 'PAST':
        backendfunctions.createvirtualcontestpast(user, contestName)

    return redirect('virtualcontest-active')


def updateproblemsdatabase(request):
    if not backendfunctions.isAccessTokenValid(request.user):
        logout(request)
        messages.info(request, 'Access Token Has Expired! Please login again to continue')
        return redirect('user-login')

    if request.user.username != 'herambpatil98' and request.user.username != 'tmw98':
        return HttpResponse('NOT DONE')

    profile = Profile.objects.filter(user=request.user).first()
    access_token = profile.access_token
    # return HttpResponse(access_token)
    response_json = backendfunctions.getContestList(access_token)

    contests_list = response_json['result']['data']['content']['contestList']
    database_contest = Contests.objects.all()
    database_problems = Problems.objects.all()
    print(contests_list)

    for contest in contests_list:
        if backendfunctions.checkContestAcceptable(contest['code']):
            continue
        if backendfunctions.checkContestExists(database_contest, contest['code']) != None:
            continue
        print(contest['code'])
        contest_json = backendfunctions.getContestJSON(access_token, contest['code'])

        if backendfunctions.isContestParent(contest_json):
            continue
        print("NOT PARENT")
        contest_code, contest_name, start_time_datetime, end_time_datetime = backendfunctions.extractContestDetails(contest_json)
        contest_obj = Contests(contest_code=contest_code, contest_name=contest_name, contest_start_time=start_time_datetime, contest_end_time=end_time_datetime)
        contest_obj.save()
        problemsList = backendfunctions.extractContestProblems(contest_json)
        print(problemsList)
        for problem in problemsList:
            if backendfunctions.checkProblemExists(database_problems, problem["problemCode"]) == None:
                print("Adding : " + problem['problemCode'])
                problem_obj = Problems(problem_code=problem["problemCode"], problem_contest=contest_obj, contest_successful_submissions=int(problem['successfulSubmissions']), contest_accuracy=int(problem['accuracy']))
                if 'LTIME' in contest_obj.contest_code:
                    problem_obj.problem_type = 'LTIME'
                elif 'COOK' in contest_obj.contest_code:
                    problem_obj.problem_type = 'COOK'
                problem_obj.save()

    practiceContest = Contests.objects.filter(contest_code='PRACTICE').first()
    database_problems = Problems.objects.all()
    # ('school', 'easy', 'medium', 'hard')
    for difficulty in ('easy', 'school', 'medium', 'hard'):
        # print(difficulty)
        problemsList = backendfunctions.getPracticeProblemsList(difficulty)
        for problem in problemsList:
            problem_code, problem_name, successful_submissions, accuracy, problem_type = backendfunctions.extractProblemDetails(problem)
            if backendfunctions.checkProblemExists(database_problems, problem_code) != None:
                problem_obj = database_problems.filter(problem_code=problem_code).first()
                problem_obj.problem_name = problem_name
                problem_obj.practice_successful_submissions = successful_submissions
                problem_obj.practice_accuracy = accuracy
                problem_obj.problem_difficulty = difficulty
                problem_obj.problem_type = problem_type
                problem_obj.save()
                print('Updated ' + problem_code)
            else:
                problem_obj = Problems(problem_code=problem_code, problem_name=problem_name, practice_successful_submissions=successful_submissions, practice_accuracy=accuracy, problem_contest=practiceContest, problem_difficulty=difficulty)
                problem_obj.save()
                print('Created ' + problem_code)


    return HttpResponse('Done')

# def downloadproblems(request):
#     access_token = request.GET.get('code')
#     print(access_token)
#     database_contest = Contests.objects.all()
#     print(database_contest)
#     database_problems = Problems.objects.all()
#     print(database_problems)
#     for contest in database_contest:
#         if backendfunctions.checkContestProblemExists(database_problems, contest) != None:
#             continue
#         # print(contest)
#         contest_json = backendfunctions.getContestJSON(access_token, contest.contest_code)
#         try:
#             problemsList = backendfunctions.extractContestProblems(contest_json)
#         except Exception as e:
#             return HttpResponse(contest_json)
#         # print(problemsList)
#         for problem in problemsList:
#             # print(problem)
#             if backendfunctions.checkProblemExists(database_problems, problem["problemCode"], contest) == None:
#                 print("Adding : " + problem['problemCode'])
#                 problem_obj = Problems(problem_code=problem["problemCode"], problem_contest=contest, contest_successful_submissions=int(problem['successfulSubmissions']), contest_accuracy=int(problem['accuracy']))
#                 problem_obj.save()

#     return HttpResponse('DONE')

@login_required(login_url='user-login')
def active(request):
    if not backendfunctions.isAccessTokenValid(request.user):
        logout(request)
        messages.info(request, 'Access Token Has Expired! Please login again to continue')
        return redirect('user-login')

    user = request.user
    profile = Profile.objects.filter(user=request.user).first()
    access_token = profile.access_token
    active_contest = backendfunctions.checkActiveVirtualContest(user)
    if active_contest == None:
        # add time decision
        messages.info(request, 'No active virtual contests! Please create one to view.')
        return redirect('virtualcontest-dashboard')

    # backendfunctions.getSubmissionStatus(access_token, active_contest)
    # return HttpResponse('<h3>Active</h3>')
    backendfunctions.updateSuccessfulSubmissions(active_contest)
    if backendfunctions.checkVirtualContestStatus(active_contest):
        messages.info(request, 'Virtual Contest has been finished. It has been moved to archived section')
        return redirect('virtualcontest-dashboard')

    problems = VirtualContestProblems.objects.filter(virtual_contest=active_contest.contest).order_by('-problem_successful_submissions')
    # problems_count = len(problems)
    # print(problems)
    context = {
        'post': active_contest,
        'problems': problems,
        'type': active_contest.contest.contestType,
        'is_active': True,
    }
    print(active_contest.contest.total_score)
    return render(request, 'virtualcontests/virtualcontest.html', context)

@login_required(login_url='user-login')
def end(request):
    if not backendfunctions.isAccessTokenValid(request.user):
        logout(request)
        messages.info(request, 'Access Token Has Expired! Please login again to continue')
        return redirect('user-login')

    user = request.user
    active_contest = backendfunctions.checkActiveVirtualContest(user)
    if active_contest == None:
        messages.info(request, 'No active virtual contests! Please create one to view.')
        return redirect('virtualcontest-dashboard')

    if backendfunctions.checkVirtualContestStatus(active_contest):
        messages.info(request, 'Virtual Contest has been finished. It has been moved to archived section')
        return redirect('virtualcontest-dashboard')

    backendfunctions.endVirtualContest(active_contest)
    return redirect('virtualcontest-dashboard')

def ranks(request):
    if not backendfunctions.isAccessTokenValid(request.user):
        logout(request)
        messages.info(request, 'Access Token Has Expired! Please login again to continue')
        return redirect('user-login')

    user = request.user
    profile = Profile.objects.filter(user=request.user).first()
    access_token = profile.access_token

    contests = Contests.objects.all()

    for contest in contests:
        if 'COOK' in contest.contest_code:
            backendfunctions.updateRanklists(access_token, contest.contest_code)
    return HttpResponse('DONE')

@login_required(login_url='user-login')
def problem(request):
    if not backendfunctions.isAccessTokenValid(request.user):
        logout(request)
        messages.info(request, 'Access Token Has Expired! Please login again to continue')
        return redirect('user-login')

    code = request.GET['code']
    if code == None:
        messages.danger('Invalid Problem! Please Select a Valid Problem')
        return redirect('virtualcontest-active')

    request.session['problemCode'] = code
    return redirect('virtualcontest-showproblem')

@login_required(login_url='user-login')
def showproblem(request):
    if not backendfunctions.isAccessTokenValid(request.user):
        logout(request)
        messages.info(request, 'Access Token Has Expired! Please login again to continue')
        return redirect('user-login')

    if 'problemCode' not in request.session:
        messages.danger('Invalid Problem! Please Select a Valid Problem')
        return redirect('virtualcontest-dashboard')
    code = request.session['problemCode']
    try:
        file = default_storage.open(code + '.html', 'r')
    except Exception as e:
        messages.danger('Invalid Problem! Please Select a Valid Problem')
        return redirect('virtualcontest-dashboard')
    htmldata = file.read()
    file.close()
    is_active = True
    active_contest = backendfunctions.checkActiveVirtualContest(request.user)
    problem = Problems.objects.filter(problem_code=code).first()
    if problem == None:
        messages.danger('Invalid Problem! Please Select a Valid Problem')
        return redirect('virtualcontest-dashboard')
    if active_contest == None:
        is_active = False
    else:
        problem = VirtualContestProblems.objects.filter(virtual_contest=active_contest.contest, problem=problem).first()
        if problem == None:
            is_active = False
    context = {
        'html': htmldata,
        'code': code,
        'is_active': is_active,
    }
    return render(request, 'virtualcontests/showproblem.html', context)

@login_required(login_url='user-login')
def archivedredirect(request):
    if not backendfunctions.isAccessTokenValid(request.user):
        logout(request)
        messages.info(request, 'Access Token Has Expired! Please login again to continue')
        return redirect('user-login')

    contestID = request.GET['id']
    if contestID == None:
        messages.danger('Invalid Contest! Please Select a Valid Contest')
        return redirect('virtualcontest-dashboard')
    request.session['contestID'] = contestID
    return redirect('virtualcontest-archived')

@login_required(login_url='user-login')
def archived(request):
    if not backendfunctions.isAccessTokenValid(request.user):
        logout(request)
        messages.info(request, 'Access Token Has Expired! Please login again to continue')
        return redirect('user-login')

    if 'contestID' not in request.session:
        messages.danger('Invalid Contest! Please Select a Valid Contest')
        return redirect('virtualcontest-dashboard')
    contestID = request.session['contestID']
    archived_contest = ArchiveVirtualContest.objects.filter(id=contestID).first()
    if not archived_contest:
        messages.danger('Invalid Contest! Please Select a Valid Contest')
        return redirect('virtualcontest-dashboard')

    problems = VirtualContestProblems.objects.filter(virtual_contest=archived_contest.contest).order_by('-problem_successful_submissions')

    context = {
        'post': archived_contest,
        'problems': problems,
        'type': archived_contest.contest.contestType,
        'is_active': False
    }
    return render(request, 'virtualcontests/virtualcontest.html', context)

def ranks(request):
    if not backendfunctions.isAccessTokenValid(request.user):
        logout(request)
        messages.info(request, 'Access Token Has Expired! Please login again to continue')
        return redirect('user-login')

    print("HELLO")
    user = request.user
    profile = Profile.objects.filter(user=request.user).first()
    access_token = profile.access_token

    contests = Contests.objects.all()

    for contest in contests:
        # if 'COOK' in contest.contest_code:
        #     backendfunctions.updateRanklistCook(access_token, contest.contest_code)
        print("UES")
        if 'LTIME' in contest.contest_code:
            print("SLDK")
            backendfunctions.updateRanklistLtime(access_token, contest.contest_code)
    return HttpResponse('DONE')
