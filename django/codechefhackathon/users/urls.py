from django.urls import path
from . import views

urlpatterns = [
    path('login/', views.user_login, name="user-login"),
    path('callback/', views.callback, name="user-callback"),
]
