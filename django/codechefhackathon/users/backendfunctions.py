from django.contrib.auth.models import User
from .models import Profile
from virtualcontests.models import AcceptedSolutions, Problems
import requests
from celery import shared_task
from django.utils import timezone

def get_token(auth_code, code_type, client_id, client_secret, redirect_uri):
    headers = {
        'content-Type': 'application/json',
    }

    data = '{"grant_type": "%s","code": "%s","client_id": "%s","client_secret": "%s","redirect_uri": "%s"}' % (code_type, auth_code, client_id, client_secret, redirect_uri)

    response = requests.post('https://api.codechef.com/oauth/token', headers=headers, data=data)
    response_json = response.json()

    access_token = response_json['result']['data']['access_token']
    token_type = response_json['result']['data']['token_type']

    return access_token, token_type

def getUserData(access_token):
    headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer %s' % (access_token),
    }
    response = requests.get('https://api.codechef.com/users/me', headers=headers)
    return response

def getUsername(userData):
    return userData['result']['data']['content']['username']

def getFullname(userData):
    return userData['result']['data']['content']['fullname']

def getCountry(userData):
    return userData['result']['data']['content']['country']['name']

def getState(userData):
    return userData['result']['data']['content']['state']['name']

def getCity(userData):
    return userData['result']['data']['content']['city']['name']

def getGlobalRankings(userData, code):
    return userData['result']['data']['content']['rankings'][code]['global']

def getCountryRankings(userData, code):
    return userData['result']['data']['content']['rankings'][code]['country']

def getRatings(userData, code):
    return userData['result']['data']['content']['ratings'][code]

def getOccupation(userData):
    return userData['result']['data']['content']['occupation']

def getOrganization(userData):
    return userData['result']['data']['content']['organization']

def userExists(username):
    return User.objects.filter(username=username).first() != None

def createUser(username):
    return User.objects.create_user(username)

def getUser(username):
    return User.objects.filter(username=username).first()

def updateUserData(user, userData, access_token):
    profile_obj = Profile.objects.filter(username=user.username).first()
    if profile_obj == None:
        createProfile(user, userData, access_token)
    else:
        updateProfile(profile_obj, userData, access_token)

def getSolvedProblemsList(userData, code):
    problemStats = userData['result']['data']['content']['problemStats']
    problemsList = []
    for key in problemStats[code]:
        for question in problemStats[code][key]:
            problemsList.append(question)

    return problemsList


def createProfile(user, userData, access_token):
    profile_obj = Profile(user=user,
        username=getUsername(userData),
        fullname=getFullname(userData),
        country=getCountry(userData),
        state=getState(userData),
        city=getCity(userData),
        allContestRankingGlobal=getGlobalRankings(userData, 'allContestRanking'),
        allContestRankingCountry=getCountryRankings(userData, 'allContestRanking'),
        longRankingGlobal=getGlobalRankings(userData, 'longRanking'),
        longRankingCountry=getCountryRankings(userData, 'longRanking'),
        shortRankingGlobal=getGlobalRankings(userData, 'shortRanking'),
        shortRankingCountry=getCountryRankings(userData, 'shortRanking'),
        ltimeRankingGlobal=getGlobalRankings(userData, 'ltimeRanking'),
        ltimeRankingCountry=getCountryRankings(userData, 'ltimeRanking'),
        allSchoolRankingGlobal=getGlobalRankings(userData, 'allSchoolRanking'),
        allSchoolRankingCountry=getCountryRankings(userData, 'allSchoolRanking'),
        longSchoolRankingGlobal=getGlobalRankings(userData, 'longSchoolRanking'),
        longSchoolRankingCountry=getCountryRankings(userData, 'longSchoolRanking'),
        shortSchoolRankingGlobal=getGlobalRankings(userData, 'shortSchoolRanking'),
        shortSchoolRankingCountry=getCountryRankings(userData, 'shortSchoolRanking'),
        ltimeSchoolRankingGlobal=getGlobalRankings(userData, 'ltimeSchoolRanking'),
        ltimeSchoolRankingCountry=getCountryRankings(userData, 'ltimeSchoolRanking'),
        allContestRating=getRatings(userData, 'allContest'),
        longRating=getRatings(userData, 'long'),
        shortRating=getRatings(userData, 'short'),
        lTimeRating=getRatings(userData, 'lTime'),
        allSchoolContestRating=getRatings(userData, 'allSchoolContest'),
        longSchoolRating=getRatings(userData, 'longSchool'),
        shortSchoolRating=getRatings(userData, 'shortSchool'),
        lTimeSchoolRating=getRatings(userData, 'lTimeSchool'),
        occupation=getOccupation(userData),
        organization=getOrganization(userData),
        access_token=access_token
    )
    profile_obj.save()
    updateUserAcceptedSolutions(user.username, userData)

def updateProfile(profile_obj, userData, access_token):
    user = profile_obj.user
    profile_obj.fullname=getFullname(userData)
    profile_obj.country=getCountry(userData)
    profile_obj.state=getState(userData)
    profile_obj.city=getCity(userData)
    profile_obj.allContestRankingGlobal=getGlobalRankings(userData, 'allContestRanking')
    profile_obj.allContestRankingCountry=getCountryRankings(userData, 'allContestRanking')
    profile_obj.longRankingGlobal=getGlobalRankings(userData, 'longRanking')
    profile_obj.longRankingCountry=getCountryRankings(userData, 'longRanking')
    profile_obj.shortRankingGlobal=getGlobalRankings(userData, 'shortRanking')
    profile_obj.shortRankingCountry=getCountryRankings(userData, 'shortRanking')
    profile_obj.ltimeRankingGlobal=getGlobalRankings(userData, 'ltimeRanking')
    profile_obj.ltimeRankingCountry=getCountryRankings(userData, 'ltimeRanking')
    profile_obj.allSchoolRankingGlobal=getGlobalRankings(userData, 'allSchoolRanking')
    profile_obj.allSchoolRankingCountry=getCountryRankings(userData, 'allSchoolRanking')
    profile_obj.longSchoolRankingGlobal=getGlobalRankings(userData, 'longSchoolRanking')
    profile_obj.longSchoolRankingCountry=getCountryRankings(userData, 'longSchoolRanking')
    profile_obj.shortSchoolRankingGlobal=getGlobalRankings(userData, 'shortSchoolRanking')
    profile_obj.shortSchoolRankingCountry=getCountryRankings(userData, 'shortSchoolRanking')
    profile_obj.ltimeSchoolRankingGlobal=getGlobalRankings(userData, 'ltimeSchoolRanking')
    profile_obj.ltimeSchoolRankingCountry=getCountryRankings(userData, 'ltimeSchoolRanking')
    profile_obj.allContestRating=getRatings(userData, 'allContest')
    profile_obj.longRating=getRatings(userData, 'long')
    profile_obj.shortRating=getRatings(userData, 'short')
    profile_obj.lTimeRating=getRatings(userData, 'lTime')
    profile_obj.allSchoolContestRating=getRatings(userData, 'allSchoolContest')
    profile_obj.longSchoolRating=getRatings(userData, 'longSchool')
    profile_obj.shortSchoolRating=getRatings(userData, 'shortSchool')
    profile_obj.lTimeSchoolRating=getRatings(userData, 'lTimeSchool')
    profile_obj.occupation=getOccupation(userData)
    profile_obj.organization=getOrganization(userData)
    profile_obj.access_token=access_token
    profile_obj.save()
    updateUserAcceptedSolutions(user.username, userData)

# @shared_task
def updateUserAcceptedSolutions(username, userData):
    # databaseProblemsList = Problems.objects.all()
    acceptedSolutions = getSolvedProblemsList(userData, 'solved')

    user = Profile.objects.filter(username=username).first().user

    acceptedSolutionsList = []
    for problem in acceptedSolutions:
        question_obj = Problems.objects.filter(problem_code=problem).first()
        acceptedExists = AcceptedSolutions.objects.filter(user=user, question=question_obj).first()
        if acceptedExists != None:
            continue
        if question_obj == None:
            continue
        acceptedSolutionsList.append(AcceptedSolutions(question=question_obj, user=user, username=user))
        if len(acceptedSolutionsList) > 500:
            AcceptedSolutions.bulk_create(acceptedSolutionsList)
            acceptedSolutionsList = []

def isAccessTokenValid(user):
    current_time = timezone.now()
    profile = Profile.objects.filter(user=user).first()
    access_token_validity_time = profile.last_login + timezone.timedelta(hours=1)
    print(current_time)
    print(access_token_validity_time)
    if current_time >= access_token_validity_time:
        return False
    return True

def updateLastLogin(user):
    profile = Profile.objects.filter(user=user).first()
    profile.last_login = timezone.now()
    profile.save()
