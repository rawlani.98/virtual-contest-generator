from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from urllib.parse import urlencode
from django.contrib.auth import authenticate, login
from . import backendfunctions
from virtualcontests.models import ActiveVirtualContest

config = {
    'client_id': 'a957ca1b0a293c1604762d7f2db6651b',
    'client_secret': '09bf6c21c28c6827f7341c965386a35b',
    'api_endpoint': 'https://api.codechef.com/',
    'authorization_code_endpoint': 'https://api.codechef.com/oauth/authorize',
    'access_token_endpoint': 'https://api.codechef.com/oauth/token',
    'redirect_uri': 'http://localhost:8000/callback/',
    'website_base_url': 'http://localhost:8000/callback/'
}

def user_login(request):
    if request.user.is_authenticated:
        if not backendfunctions.isAccessTokenValid(request.user):
            logout(request)
            messages.info(request, 'Access Token Has Expired! Please login again to continue')
            return redirect('user-login')
        else:
            return redirect('virtualcontest-dashboard')
    if request.method == 'POST':
        params = {'response_type': 'code', 'client_id': config['client_id'], 'redirect_uri': config['redirect_uri'], 'state': 'xyz'}
        generated_url = config['authorization_code_endpoint'] + '?' + urlencode(params)
        return redirect(generated_url)
    return render(request, 'users/login.html')

def callback(request):
    if request.user.is_authenticated:
        if not backendfunctions.isAccessTokenValid(request.user):
            logout(request)
            messages.info(request, 'Access Token Has Expired! Please login again to continue')
            return redirect('user-login')
        else:
            return redirect('dashboard/')

    authorization_code = request.GET.get('code')
    state = request.GET.get('state')

    if authorization_code == None:
        messages.error(request, 'Error Logging In! Try Again!')
        return redirect('user-login')

    access_token, token_type = backendfunctions.get_token(authorization_code, 'authorization_code', config['client_id'], config['client_secret'], config['redirect_uri'])

    # return HttpResponse(access_token)
    userData = backendfunctions.getUserData(access_token)
    userDataJSON = userData.json()
    username = backendfunctions.getUsername(userDataJSON)

    if not backendfunctions.userExists(username):
        user = backendfunctions.createUser(username)
    else:
        user = backendfunctions.getUser(username)
    backendfunctions.updateUserData(user, userDataJSON, access_token)

    backendfunctions.updateLastLogin(user)
    login(request, user)
    return redirect('virtualcontest-dashboard')
    # return HttpResponse('DONE')
