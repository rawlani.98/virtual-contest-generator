# Generated by Django 2.1.1 on 2018-09-17 07:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_auto_20180917_0747'),
    ]

    operations = [
        migrations.RenameField(
            model_name='profile',
            old_name='longRankingGloabl',
            new_name='longRankingGlobal',
        ),
    ]
